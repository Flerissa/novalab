/**
 * @file angles.c
 * Функції конвертування градусів у радіани та навпаки
 * @author Поліна Федюкіна
 * @version 1.0.0
 * @date 2020.03.03
 */

#include "../inc/angles.h"

double radians_to_degrees(double radians)
{
	double degrees = radians * 180 / M_PI;
	return degrees;
}

double degrees_to_radians(double degrees)
{
	double radians = degrees * M_PI / 180;
	return radians;
}
