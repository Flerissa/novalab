/**
 * @file parser.c
 * Функція конвертування строки в структуру
 * @author Polina Fediukina
 * @version 1.0.0
 * @date 2020.05.24
 */

#include "../inc/parser.h"
#include <regex.h>

struct point_cartesian parser_point(char* raw_data)
{
	struct point_cartesian point;
    point.x = 0;
    point.y = 0;

    char* rText = "[xy]=[0-9]+&[yx]=[0-9]+$";   
    int res;
    regex_t regex;                              
    regmatch_t group[1];
    regcomp(&regex, rText, REG_EXTENDED);
    res = regexec(&regex, raw_data, 1, group, 0);
    if (res == REG_NOMATCH)
    {
        fprintf(stderr, "Can't parse line: '%s'\n", raw_data);
        return point;
    }
    regfree(&regex);    

    char rX[10] = "x=[0-9]+";       
    char rY[10] = "y=[0-9]+";     

    char _x[5], _y[5];

    regcomp(&regex, rX, REG_EXTENDED);
    regexec(&regex, raw_data, 1, group, 0);
    for (int i = group[0].rm_so + 2, j = 0; i < group[0].rm_eo; i++, j++)
        _x[j] = raw_data[i];          
    regfree(&regex);

    regcomp(&regex, rY, REG_EXTENDED); 
    regexec(&regex, raw_data, 1, group, 0);
    for (int i = group[0].rm_so + 2, j = 0; i < group[0].rm_eo; i++, j++)
        _y[j] = raw_data[i]; 
    regfree(&regex);

    point.x = atoi(_x);
    point.y = atoi(_y);

    return point;
}
