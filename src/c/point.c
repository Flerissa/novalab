/**
 * @file point.c
 * Функції конвертування положення точки з полярної систему у декартову та навпаки
 * @author Поліна Федюкіна
 * @version 1.0.0
 * @date 2020.03.03
 */

#include "../inc/point.h"

struct point_cartesian polar_to_cartesian(struct point_polar point)
{
	struct point_cartesian result;
	result.x = point.r * cos(degrees_to_radians(point.a));
	result.y = point.r * sin(degrees_to_radians(point.a));
	if (!point.a && !point.r) {
		result.y = 0;
		result.x = 0;
	}
	return result;
}

struct point_polar cartesian_to_polar(struct point_cartesian point)
{
	struct point_polar result;
	result.r = sqrt(point.x * point.x + point.y * point.y);
	result.a = 1 / tan(point.y / point.x);
	if (!point.x && !point.y) {
		result.a = 0;
		result.r = 0;
	}
	return result;
}
