/**
 * @file sort.c
 * Функції сортування
 * @author Поліна Федюкіна 
 * @version 1.0.0
 * @date 2020.05.26
 */

#include "../inc/sort.h"


void sort_array_desc_ranged(int data[], int size, int low_range, int high_range)
{
    int l = low_range, h = high_range;
    if(low_range >= size) l = 0;
    if(high_range >= size || high_range < 0) h = size - 1;
    if(h < l || l < 0 || l == h)
    {
        printf("%i", data[0]);
        for(int i = 1; i < size; i++)
            printf(", %i", data[i]);

        return;
    }
    int t;

    int *arr = (int*)malloc(sizeof(int) * size);
    for(int i = 0; i < size; i++)
        arr[i] = data[i];

    for (int i = l; i < h; i++)
    {
        for (int j = h; j > i; j--)
        
            if (arr[j - 1] > arr[j])
            {
                t = arr[j - 1];
                arr[j - 1] = arr[j];
                arr[j] = t;
            }
    }
    printf("%i", arr[0]);
    for(int i = 1 ; i < size; i++)
            printf(", %i", arr[i]);
    free(arr);
}
