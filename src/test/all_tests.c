/**
 * @file all_tests.c
 * Збирає всі тести
 * @author Поліна Федюкіна
 * @version 1.0.0
 * @date 2020.03.03
 */


#include <cgreen/cgreen.h>

TestSuite *angles_test();
TestSuite *point_test();
TestSuite *parser_test();
TestSuite *segment_test();
TestSuite *sort_test();
TestSuite *matrix_test();

int main(int argc, char **argv)
{
	TestSuite *suite = create_test_suite();
	add_suite(suite, angles_test());
	add_suite(suite, point_test());
	add_suite(suite, parser_test());
	add_suite(suite, segment_test());
	add_suite(suite, sort_test());
	add_suite(suite, matrix_test());
	if (argc > 1) {
		return run_single_test(suite, argv[1], create_text_reporter());
	}
	return run_test_suite(suite, create_text_reporter());
}
