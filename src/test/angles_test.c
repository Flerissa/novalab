/**
 * @file angles_test.c
 * Тестування функцій з angles.c
 * @author Поліна Федюкіна
 * @version 1.0.0
 * @date 2020.03.03
 */

#include <cgreen/cgreen.h>

#include "../inc/angles.h"

Describe(Angles);
BeforeEach(Angles)
{
}
AfterEach(Angles)
{
}

Ensure(Angles, returns_radians)
{
	const int tests = 6;
	double input[tests] = { 57.29, 0, 212.95, 10, 360, 1000 };
	double result[tests] = { 1, 0, 3.71, 0.17, 6.28, 17.45 };
	significant_figures_for_assert_double_are(2);
	for (unsigned i = 0; i < tests; i++) {
		assert_that_double(degrees_to_radians(input[i]),
				   is_equal_to_double(result[i]));
	}
}

Ensure(Angles, returns_degrees)
{
	const int tests = 6;
	double input[tests] = { 1, 0, 10, 0.17, 6.28, 4.89 };
	double result[tests] = { 57.29, 0, 572.95, 9.74, 359.81, 280.17 };
	significant_figures_for_assert_double_are(2);
	for (unsigned i = 0; i < tests; i++) {
		assert_that_double(radians_to_degrees(input[i]),
				   is_equal_to_double(result[i]));
	}
}

TestSuite *angles_test()
{
	TestSuite *suite = create_test_suite();
	add_test_with_context(suite, Angles, returns_radians);
	add_test_with_context(suite, Angles, returns_degrees);
	return suite;
}
