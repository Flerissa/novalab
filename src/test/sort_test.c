/**
 * @file sort_test.c
 * Тестування функцій з sort.c
 * @author Поліна Федюкіна
 * @version 1.0.0
 * @date 2020.03.03
 */

#include <cgreen/cgreen.h>
#include "../inc/sort.h"

Describe(Sort);
BeforeEach(Sort)
{
}
AfterEach(Sort)
{
}

Ensure(Sort, returns_sort)
{
	const int tests = 5;
    const short size = 10;
	int data[size] = {1, -4, -5, -3, 2, 5, 10, 3, 6, 5};
    int ranges[tests][2] = {{5,1}, {100,4}, {5,-1}, {0 , 9}, {4,7}};
    for(int i = 0; i < tests; i++)
    {
        printf("(x = [%i", data[0]);
        for(int i = 1; i < size; i++)
            printf(", %i", data[i]);

        printf("], l = %i, h = %i)  =>  [", ranges[i][0], ranges[i][1]);
        sort_array_desc_ranged(data, size, ranges[i][0], ranges[i][1]);
        printf("]\n");
    }
}

TestSuite *sort_test()
{
	TestSuite *suite = create_test_suite();
	add_test_with_context(suite, Sort, returns_sort);
	return suite;
}
