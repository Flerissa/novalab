/**
 * @file segment_test.c
 * Тестовий файл для segment.c
 * @author Поліна Федюкіна
 * @version 1.0.0
 * @date 2020.05.19
 */

#include <cgreen/cgreen.h>

#include "../inc/segment.h"

Describe(Segment);
BeforeEach(Segment)
{
}
AfterEach(Segment)
{
}

Ensure(Segment, returns_length)
{
	const int tests = 4;
	double input[tests][4] = { {2, 1, 23, 4}, {-2, 1, 0, 4}, {1, 1, 1, 1}, {6, -6, -6, 6} };
	double result[tests] = { 21.21, 3.6, 0, 16.97};
    struct line_segment s;
    double res;

    significant_figures_for_assert_double_are(2);
	for (unsigned i = 0; i < tests; i++)
	{
        s.A.x = input[i][0];
        s.A.y = input[i][1];
        s.B.x = input[i][2];
        s.B.y = input[i][3];
        res = get_length(s);
        assert_that_double(res, is_equal_to_double(result[i]));
    }
}

Ensure(Segment, returns_is_point_on_line)
{
	const int tests = 4;
	double input[tests][4] = { {2, 1, 23, 4}, {2,1,-2,-1}, {5, -1, -20, -1}, {2,1,-2,-1} };
    double point[tests][2] = {{0,0}, {0,0}, {4,10}, {1.5, 1.5}};
	double result[tests] = { 0, 1, 0, 0};
    struct line_segment s;
    struct point_cartesian p;

    significant_figures_for_assert_double_are(2);
	for (unsigned i = 0; i < tests; i++)
	{
        s.A.x = input[i][0];
        s.A.y = input[i][1];
        s.B.x = input[i][2];
        s.B.y = input[i][3];
        p.x = point[i][0];
        p.x = point[i][1];
        assert_that_double(is_point_on_line(p, s), is_equal_to_double(result[i]));
    }
}

TestSuite *segment_test()
{
	TestSuite *suite = create_test_suite();
	//add_test_with_context(suite, Angles, returns_radians);
	add_test_with_context(suite, Segment, returns_length);
    add_test_with_context(suite, Segment, returns_is_point_on_line);
	return suite;
}
