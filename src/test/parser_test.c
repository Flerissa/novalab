/**
 * @file parser_test.c
 * Тестування функцій з parser.c
 * @author Поліна Федюкіна
 * @version 1.0.0
 * @date 2020.03.03
 */

#include <cgreen/cgreen.h>
#include "../inc/parser.h"

Describe(Parser);
BeforeEach(Parser)
{
}
AfterEach(Parser)
{
}

Ensure(Parser, returns_parsing)
{
    const int tests = 6;
    double result[tests][2] ={{12, 25}, {25, 12}, {0, 0}, {0, 0}, {0, 0}, {0, 0}};
    char* input[tests] = {"point:x=12&y=25", "point:y=12&x=25", "point:y=&x=25","point:y=10", "point:y=10&", "point:y=10&x=5x"};
    significant_figures_for_assert_double_are(0);
    struct point_cartesian point;
    FILE* f = freopen("output/test/errors.txt", "w", stderr);
    char text[256];
    for(int i = 0; i < tests; i++)
    {
        point = parser_point(input[i]);
        assert_that_double(point.x, is_equal_to_double(result[i][0]));
        assert_that_double(point.y, is_equal_to_double(result[i][1]));
        
    }
    fclose(f);
    f = fopen("output/test/errors.txt", "r");
    while(!feof(f))
    {
        fgets(text, 256, f);
        printf("%s", text);
    }
    fclose(f);
}

TestSuite *parser_test()
{
	TestSuite *suite = create_test_suite();
	add_test_with_context(suite, Parser, returns_parsing);
	return suite;
}
