/**
 * @file parser.h
 * Заголовний файл для parser.c. Містить прототипи функцій та необхідні бібліотеки.
 * @author Поліна Федюкіна
 * @version 1.0.0
 * @date 2020.05.19
 */

#include <stdlib.h>
#include <stdio.h>
#include "point.h"

/**
 * Переводить строчку до структури point_cartesian
 * @param raw_data - строка з даними
 * @return структуру point_cartesian
 */
struct point_cartesian parser_point(char* raw_data);
