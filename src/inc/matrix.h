/**
 * @file matrix.h
 * Заголовний файл для matrix.c
 * @author Поліна Федюкіна
 * @version 1.0.0
 * @date 2020.06.02
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/**
 * @struct matrix
 * @brief Описує матрицю
 * @var rows
 * @var cols
 * @var data 
 */
struct matrix
{
    int rows; ///< строки
    int cols; ///< стовпці
    float *data; ///< одномірний масив даних
};


/**
 * Виводить одномірний масив у вигляді двомірного
 * @param array - масив чисел
 * @param rows - строки
 * @param cols - стовпці
 */
void print_2d_array(int * array, int rows, int cols);

/**
 * Виводить матрицю
 * @param source - матриця
 */
void print_matrix(struct matrix source);

/**
 * Копіює матрицю
 * @param source - матриця
 * @return копію матриці
 */
struct matrix copy_matrix(struct matrix source);

/**
 * Множить матрицю на число
 * @param source - матриця
 * @param lambda - число
 * @return помножену матрицю на число
 */
struct matrix mul_matrix_by_value(struct matrix source, double lamda);

/**
 * Сумує матриці
 * @param source1 - матриця 1
 * @param source2 - матриця 2
 * @return суму матриць
 */
struct matrix sum_matrix(struct matrix source1, struct matrix source2);

/**
 * Множить матриці
 * @param source1 - матриця 1
 * @param source2 - матриця 2
 * @return добуток матриць
 */
struct matrix mul_matrix(struct matrix source1, struct matrix source2);

/**
 * Транспонує матрицю
 * @param source - матриця
 * @return транспоновану матрицю
 */
struct matrix transpose_matrix(struct matrix source);
/**
 * Шукає детермінант матриці
 * @param source - матриця
 * @return детермінант матриці
 */
double get_matrix_determinant(struct matrix source);


/**
 * Шукає зворотню матрицю
 * @param source - матриця
 * @return зворотню матрицю
 */
struct matrix get_inverse_matrix(struct matrix source);
