/**
 * @file point.h
 * Заголовний файл для point.c. Містить прототипи функцій та необхідні бібліотеки
 * @author Polina Fediukina
 * @version 1.0.0
 * @date 2020.03.03
 */

#include "angles.h"
#include <math.h>


/**
 * @struct point_polar
 * @brief Описує положення точки в полярній системі кординат
 * @var r - радіус
 * @var a - кут
 */
struct point_polar {
	double r; ///< радіус
	double a;///< кут
};

/**
 * @struct point_cartesian
 * @brief Описує положення точки в декартовій системі кординат
 * @var x
 * @var y 
 */
struct point_cartesian {
	double x;///< значення по осі х
	double y;///< значення по осі у
};


/**
 * Переводить полярні кординати на декартові
 * @param point - положення в полярній системі кординат
 * @return положення в декартовій системі
 */
struct point_cartesian polar_to_cartesian(struct point_polar point);

/**
 * Переводить декартові кординати на полярні
 * @param point - положення в декартовій системі
 * @return положення в полярній системі кординат
 */
struct point_polar cartesian_to_polar(struct point_cartesian point);
