/**
 * @file sort.h
 * Заголовний файл для sort.c. Містить прототипи функцій та необхідні бібліотеки.
 * @author Поліна Федюкіна
 * @version 1.0.0
 * @date 2020.05.19
 */

#include <stdio.h>
#include <stdlib.h>


/**
 * Сортує масив цілих чисел залежно від граниь сортування
 * @param data - масив цілих чисел
 * @param size - розмір масиву
 * @param low_range - нижня межа сортування
 * @param high_range - верхня межа сортування
 */
void sort_array_desc_ranged(int data[], int size, int low_range, int high_range);
