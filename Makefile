CC=clang
CFLAGS= -lm -Werror -Wextra
MAIN=src/main/main.c
FUNC=src/c/*.c
TEST=src/test/*.c
EXEC=output/dist/task02
EXEC_TEST=output/test/test

all: clean output build_main doxygen build_test 

output: 
	mkdir -p output/dist output/doxygen output/test

build_main:
	rm -f $(EXEC)
	$(CC) $(CFLAGS) $(MAIN) $(FUNC) -o $(EXEC)
	clang-tidy $(FUNC) $(MAIN)
	
build_test: 
	$(CC) $(CFLAGS) -lcgreen $(TEST) $(FUNC) -o $(EXEC_TEST)
	./$(EXEC_TEST)
	rm -r $(EXEC_TEST)
	
doxygen:
	doxygen
	
test:
	valgrind ./$(EXEC)
	
clean: 
	rm -r -f output
